#!/usr/bin/env python3

class Calculator:
    def __init__(self):
        self.total = 0
     
    def display(self):
        print(self.total)
        
    def add(self, num1, num2):
        result = num1 + num2
        self.total = result
        return self.total
    
    def sub(self, num1, num2):
        result = num1 - num2
        self.total = result
        return self.total
    
    def mul(self, num1, num2):
        result = num1 * num2
        self.total = result
        return self.total
    
    def div(self, num1, num2):
        try:
            result = num1 / num2
            self.total = result
            return self.total
        except ZeroDivisionError:
            print('integer cannot divided by zero')
            result = 0
            
    #     if operator == 'log':
    #         try:
    #            result = math.log(a) / math.log(b)
    #        except (ValueError, TypeError, ZeroDivisionError):
    #            print('Please double-check your values')
    #            result = 0
    #     else:
    #         print('No errors!')
    #     finally:
    #        print('Finally!')
    # return result
    



