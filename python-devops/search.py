#!/usr/bin/env python3

import re
import sys

file = open(sys.argv[1], 'r')
CRED = '\033[91m'
CEND = '\033[0m'

for line in file:
    if re.search(sys.argv[2], line):
        print(CRED + line +CEND, end='')
