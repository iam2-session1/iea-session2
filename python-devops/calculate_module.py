#!/usr/bin/env python3

import sys
import math
from mymodule import calculate

def calculate_args(a, b, operator):
    print('Program arguments', sys.argv)



if __name__ == '__main__':
    print('Running as a script')
    for idx, arg in enumerate(sys.argv):
        print(f'arg {idx} is {arg}')


    if len(sys.argv) != 4:
        print('Usage: calculate_argv OPERAND')
        sys.exit(-1)

    try:
        a = float(sys.argv[1])
        b = float(sys.argv[2])
        operator = sys.argv[3]
    except ValueError:
        print('Invalid parameter! Please supply numeric values')
    else:
        print('Result:', calculate(a,b,operator))
else:
    print('Imported')


