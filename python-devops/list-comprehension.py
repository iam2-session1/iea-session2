nums = [1,2,3,4,5,6,7,8,9,10]

# i want 'n' for each 'n' in nums
my_list= []
for n in nums:
    if n%2 == 0:
        my_list.append()
print(my_list)

# list comprehension
my_list = [ n for n in nums if n%2 == 0]
print(my_list)


# i want aa (letter, num) pair for each letter in 'abcd' and each number in '0123'
my_list = []
for letter in 'abcd':
    for num in range(4):
        my_list.append((letter, num))
print(my_list)



# list comprehension
my_list = [(letter, num) for letter in 'abcd' for num in range(4)]
print(my_list)

#dictionary comprehensions
names = ['Bruce', 'Clark', 'Peter', 'Logan', 'Wade']
heros = ['Batman', 'Superman', 'Spiderman', 'wolverine', 'Deadpool']
print zip(names, heros)

#i want a dict{'name':'hero'} for each name, hero in zip(names, heros)
my_dict = {}
for name, hero in zip(names, heros):
    my_dict[name] = hero
print(my_dict)



# dict comprehension
my_dict = {name: hero for name, hero in zip (names, heros)}


#if name not equal to peter
my_dict = {name: hero for name, hero in zip (names, heros) if name != 'Peter'}


#set comprehensions
nums = [1,1, 2,1 2,3,4,5,5,7,6,7,8,9,9,10]
my_set = set()
for n in nums:
    my_set.add(n)
print(my_set)


#set comprehensions
my_set = {n for n in nums}
print(my_set)
