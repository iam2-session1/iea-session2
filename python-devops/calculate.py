#!/usr/bin/env python3

import sys
import math

def calculate_args(a, b, operator):
    print('Program arguments', sys.argv)


def calculate(a, b, operator):
    if operator == '+':
        result = a + b
    elif operator == '-':
        result = a - b
    elif operator == '*':
        result = a * b
    elif operator == '/': 
        try:
            result = a / b
        except ZeroDivisionError:
            print('integer cannot divided by zero')
            result = 0
    elif operator == 'log':
        try:
            result = math.log(a) / math.log(b)
        except (ValueError, TypeError, ZeroDivisionError):
            print('Please double-check your values')
            result = 0
        else:
            print('No errors!')
        finally:
            print('Finally!')
    return result
    

if __name__ == '__main__':
    print('Running as a script')
    for idx, arg in enumerate(sys.argv):
        print(f'arg {idx} is {arg}')


    if len(sys.argv) != 4:
        print('Usage: calculate_argv OPERAND')
        sys.exit(-1)

    try:
        a = float(sys.argv[1])
        b = float(sys.argv[2])
        operator = sys.argv[3]
    except ValueError:
        print('Invalid parameter! Please supply numeric values')
    else:
        print('Result:', calculate(a,b,operator))
else:
    print('Imported')


