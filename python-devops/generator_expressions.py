#!/usr/bin/env python3

nums = [1,2,3,4,5,6,7,8,9,10]

my_gen = (n*n for n in nums)

for i in my_gen:
    print(i)
