#!/usr/bin/env python3

x = int(input('Enter the number of rows'))
y = 2*x -1
n = y

while n >= 1:
    space = y - n // 2
    for j in range (1, space + 1):
        print(" ", end='')
    for i in range(1, n + 1):
        print("*", end='')
    for k in range(1, space + 1):
        print(" ", end='')
    print()    
    n = n - 2    

