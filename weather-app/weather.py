#! /usr/bin/env python3
import requests, time, sys

DARK_SKY_SECRET_KEY='1d8c58ed1d54f96f939e706c788650f1'

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    str: city_state
     """
    
    location = requests.get('https://ipvigilante.com/')
    data = location.json()

    latitude = data['data']['latitude']
    longitude = data['data']['longitude']
    city = data['data']['city_name']
    state = data['data']['subdivision_1_name']
    city_state = f'{city}, {state}'

    # print(f'Longitude: {longitude} Latitude: {latitude}')
  
    return longitude, latitude, city_state

def get_current_forecast(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    """ 
    summary
    temp
    high
    low
    humidity
    feels like"""

    try:
        api_call = requests.get(f'https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude}')
        api_call.raise_for_status()   
    except requests.exceptions.HTTPError as e:
        print(e)
        return None
    
    data = api_call.json()
    daily = data['daily']['data'][0]


    summary = data['currently']['summary']
    temperature = data['currently']['temperature']
    high_temp = daily['temperatureHigh']
    low_temp = daily['temperatureLow']
    humidity = data['currently']['humidity']
    feels_like = data['currently']['apparentTemperature']
    
    # print(temperature)

    return temperature, summary, high_temp, low_temp, humidity, feels_like

def get_time_machine(longitude, latitude, time_date = 'Today'):
    if time_date == 'Today':
        try:
            temperature, summary, high_temp, low_temp, humidity, feels_like = get_current_forecast(longitude, latitude) 
        except TypeError:
            print('Error calling API.')
            return None

    else:
        try:
            api_call = requests.get(f'https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude},{time_date}')
            api_call.raise_for_status()   
        except requests.exceptions.HTTPError as e:
            print(e)
            return None

        data_json = api_call.json()
        currently = data_json['hourly']['data'][0] # Goes into the hourly dict
        daily = data_json['daily']['data'][0]
        
        summary = currently['summary']
        temperature = currently['temperature']
        high_temp = daily['temperatureHigh']
        low_temp = daily['temperatureLow']
        humidity = currently['humidity']
        feels_like = currently['apparentTemperature']
        
    return temperature, summary, high_temp, low_temp, humidity, feels_like



def print_forecast(city_state, temp, summary, high_temp, low_temp, humidity, feels_like,time_date):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
        
    print(f"{time_date} forecast in {city_state}")
    print()
    print(summary)
    print()
    print(f'Temperature: {temp}')
    print(f"Today's High: {high_temp}")
    print(f"Today's Low: {low_temp}")
    print(f'Humidity: {humidity}')
    print(f'Feels like: {feels_like}')

def convert_time(time_date):
    try:
        time_int = int(time_date) # Need to make time an int to convert it
    except ValueError:
        print('Please use an interger value')
        time_int = 0 
    finally:
        time_date_string = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_int))
    return time_int, time_date_string

if __name__ == "__main__":
    if len(sys.argv) > 1:
        time_date, time_string = convert_time(sys.argv[1])
        
    else:
        time_date = 'Today'
        time_string = 'Today'

    longitude, latitude, city_state = get_location()
    try:
        temp,summary, high_temp, low_temp, humidity, feels_like = get_time_machine(longitude, latitude, time_date)
        print_forecast(city_state, temp, summary, high_temp, low_temp, humidity, feels_like, time_string)
    except TypeError:
        print('Please check for errors.')
