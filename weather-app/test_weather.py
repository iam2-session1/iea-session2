import pytest
import weather

class MockResponse:
    def __init__(self,data, status_code):
        self.data = data
        self.status_code = status_code

    def json(self):
        return self.data

    def raise_for_status(self):
        return self.status_code

def mock_get_location(*args):
    # Grabbed an example jsonm return from https://ipvigilante.com/
    # Can clean up mock_location data 
    data = {"status":"success","data":{"ipv4":"3.93.156.9","continent_name":"North America","country_name":"United States","subdivision_1_name":"Connecticut","subdivision_2_name":None,"city_name":"Fairfield","latitude":"41.14120","longitude":"-73.26370"}}
    return MockResponse(data, 200)

def test_get_location(mocker):
    mocker.patch('requests.get', mock_get_location )
    longitude, latitude, city_state = weather.get_location()

    # latitude = data['data']['latitude'] 
    # longitude = data['data']['longitude']
    # city = data['data']['city_name']
    # state = data['data']['subdivision_1_name']
    # city_state = f'{city},{state} '

    assert latitude == '41.14120'
    assert longitude == '-73.26370'
    assert city_state == 'Fairfield, Connecticut'

def mock_api_call(*args):
    api_call = {"currently": {"summary": "Drizzle","temperature": 66.1,"apparentTemperature": 66.31,"humidity": 0.83},"hourly": {"data": [{"summary": "Mostly Cloudy","temperature": 65.76,"apparentTemperature": 66.01,"humidity": 0.85},]},"daily": {"data": [{"temperatureHigh": 66.35,"temperatureLow": 41.28},]},}
    return MockResponse(api_call, 200)

def test_get_current_forcast(mocker):
    mocker.patch('requests.get', mock_api_call )
    temperature, summary, high_temp, low_temp, humidity, feels_like = weather.get_current_forecast('-73.26370', '41.14120')

    assert temperature == 66.1
    assert summary == "Drizzle"
    assert high_temp == 66.35
    assert low_temp == 41.28
    assert humidity == 0.83
    assert feels_like == 66.31

def test_get_time_machine_current(mocker):
    mocker.patch('requests.get', mock_api_call )

    temp,summary, high_temp, low_temp, humidity, feels_like = weather.get_time_machine('-73.26370', '41.14120', 'Today')

    assert temp == 66.1
    assert summary == "Drizzle"
    assert high_temp == 66.35
    assert low_temp == 41.28
    assert humidity == 0.83
    assert feels_like == 66.31

def test_get_time_machine_time(mocker):
    mocker.patch('requests.get', mock_api_call )

    temp,summary, high_temp, low_temp, humidity, feels_like = weather.get_time_machine('-73.26370', '41.14120', '1589403419')

    assert temp == 65.76
    assert summary == 'Mostly Cloudy'
    assert high_temp == 66.35
    assert low_temp == 41.28
    assert humidity == 0.85
    assert feels_like == 66.01

def test_convert_time_int():
    time_int, time_string = weather.convert_time('1589403419')

    assert time_int == 1589403419
    assert time_string == '2020-05-13 15:56:59'

def test_convert_time_string():
    time_int, time_string = weather.convert_time('abc')

    assert time_int == 0
    assert time_string == '1969-12-31 18:00:00'
